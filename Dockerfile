FROM openjdk:12-alpine
VOLUME /tmp
ADD /build/libs/*.jar demoApp.jar
EXPOSE 5000
ENTRYPOINT exec java -jar demoApp.jar
